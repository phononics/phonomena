import numpy as np

class Solver:

    def __init__(self, grid):
        self.g = grid
        return

    def update_ricker(self, g, tt):

        # define constants for ricker wave
        ppw = 20
        source_delay = 0

        # create ricker wave
        arg = (np.pi*((g.sc*tt - source_delay)/ppw - 1.0)) ** 2
        ricker = (1 - 2 * arg) * np.exp(-arg)
        return ricker

    def update_delta(self, tt):

        source_delay = 0
        if tt == source_delay:
            delta = 1
        else:
            delta = 0
        return delta

    def update_sine(self, g, frequency, tt):

        f0 = frequency
        phi = 0
        sine = np.sin(2*np.pi*f0*tt*g.dt)
        return sine

    def update_T(self, g, m):

        g.T1[1:-1,1:-1,1:-1] = \
          m.C[1:-1,1:-1,1:-1,0,0]*g.u2T*(g.ux[1:,1:-1,1:-1] - g.ux[:-1,1:-1,1:-1]) \
        + m.C[1:-1,1:-1,1:-1,0,1]*g.u2T*(g.uy[1:-1,1:,1:-1] - g.uy[1:-1,:-1,1:-1]) \
        + m.C[1:-1,1:-1,1:-1,0,2]*g.u2T*(g.uz[1:-1,1:-1,1:] - g.uz[1:-1,1:-1,:-1])

        g.T2[1:-1,1:-1,1:-1] = \
          m.C[1:-1,1:-1,1:-1,1,0]*g.u2T*(g.ux[1:,1:-1,1:-1] - g.ux[:-1,1:-1,1:-1]) \
        + m.C[1:-1,1:-1,1:-1,1,1]*g.u2T*(g.uy[1:-1,1:,1:-1] - g.uy[1:-1,:-1,1:-1]) \
        + m.C[1:-1,1:-1,1:-1,1,2]*g.u2T*(g.uz[1:-1,1:-1,1:] - g.uz[1:-1,1:-1,:-1])

        g.T3[1:-1,1:-1,1:-1] = \
          m.C[1:-1,1:-1,1:-1,2,0]*g.u2T*(g.ux[1:,1:-1,1:-1] - g.ux[:-1,1:-1,1:-1]) \
        + m.C[1:-1,1:-1,1:-1,2,1]*g.u2T*(g.uy[1:-1,1:,1:-1] - g.uy[1:-1,:-1,1:-1]) \
        + m.C[1:-1,1:-1,1:-1,2,2]*g.u2T*(g.uz[1:-1,1:-1,1:] - g.uz[1:-1,1:-1,:-1])

        g.T4[1:-1,:,:] = m.C[1:-1,1:,1:,3,3]*g.u2T*( \
                                   (g.uy[1:-1,:,1:] - g.uy[1:-1,:,:-1]) \
                                 + (g.uz[1:-1,1:,:] - g.uz[1:-1,:-1,:]))

        g.T5[:,1:-1,:] = m.C[1:,1:-1,1:,4,4]*g.u2T*( \
                                   (g.ux[:,1:-1,1:] - g.ux[:,1:-1,:-1]) \
                                 + (g.uz[1:,1:-1,:] - g.uz[:-1,1:-1,:]))

        g.T6[:,:,1:-1] = m.C[1:,1:,1:-1,5,5]*g.u2T*( \
                                    (g.ux[:,1:,1:-1] - g.ux[:,:-1,1:-1]) \
                                  + (g.uy[1:,:,1:-1] - g.uy[:-1,:,1:-1]))

    def update_T_BC(self, g, m):

        # self.apply_T_pbc(g)
        self.apply_T_tfbc(g, m)

    def apply_T_pbc(self, g):

        g.T1[:,0,:] = g.T1[:,-2,:]
        g.T2[:,0,:] = g.T2[:,-2,:]
        g.T3[:,0,:] = g.T3[:,-2,:]
        # g.T4[:,0,:] = g.T4[:,-2,:]
        g.T5[:,0,:] = g.T5[:,-2,:]
        # g.T6[:,0,:] = g.T6[:,-2,:]

        # g.T1[:,-1,:] = g.T1[:,1,:]
        # g.T2[:,-1,:] = g.T2[:,1,:]
        # g.T3[:,-1,:] = g.T3[:,1,:]
        g.T4[:,-1,:] = g.T4[:,1,:]
        # g.T5[:,-1,:] = g.T5[:,1,:]
        g.T6[:,-1,:] = g.T6[:,1,:]

    def apply_T_tfbc(self, g, m):

        g.T1[1:-1,1:-1,0] = \
          m.C[1:-1,1:-1,0,0,0]*g.u2T*(g.ux[1:,1:-1,0] - g.ux[:-1,1:-1,0]) \
        + m.C[1:-1,1:-1,0,0,1]*g.u2T*(g.uy[1:-1,1:,0] - g.uy[1:-1,:-1,0]) \
        + m.C[1:-1,1:-1,0,0,2]*g.u2T*(g.uz[1:-1,1:-1,0] - 0)

        g.T2[1:-1,1:-1,0] = \
          m.C[1:-1,1:-1,0,1,0]*g.u2T*(g.ux[1:,1:-1,0] - g.ux[:-1,1:-1,0]) \
        + m.C[1:-1,1:-1,0,1,1]*g.u2T*(g.uy[1:-1,1:,0] - g.uy[1:-1,:-1,0]) \
        + m.C[1:-1,1:-1,0,1,2]*g.u2T*(g.uz[1:-1,1:-1,0] - 0)

        g.T3[1:-1,1:-1,0] = 0

        g.T4[1:-1,:,0] = m.C[1:-1,1:,0,3,3]*g.u2T*( \
                                   (g.uy[1:-1,:,1] - g.uy[1:-1,:,0]) \
                                 + (g.uz[1:-1,1:,0] - g.uz[1:-1,:-1,0]))

        g.T5[:,1:-1,0] = m.C[1:,1:-1,0,4,4]*g.u2T*( \
                                   (g.ux[:,1:-1,1] - g.ux[:,1:-1,0]) \
                                 + (g.uz[1:,1:-1,0] - g.uz[:-1,1:-1,0]))

        g.T6[:,:,0] = m.C[1:,1:,0,5,5]*g.u2T*( \
                                    (g.ux[:,1:,0] - g.ux[:,:-1,0]) \
                                  + (g.uy[1:,:,0] - g.uy[:-1,:,0]))

    def update_u(self, g):

        g.ux_new[:,1:-1,1:-1] = 2*g.ux[:,1:-1,1:-1] \
            - g.ux_old[:,1:-1,1:-1] \
            + g.T2u[1:,1:-1,1:-1]*( \
              (g.T1[1:,1:-1,1:-1] - g.T1[:-1,1:-1,1:-1]) \
            + (g.T5[:,1:-1,1:] - g.T5[:,1:-1,:-1]) \
            + (g.T6[:,1:,1:-1] - g.T6[:,:-1,1:-1]))

        g.uy_new[1:-1,:,1:-1] = 2*g.uy[1:-1,:,1:-1] \
            - g.uy_old[1:-1,:,1:-1] \
            + g.T2u[1:-1,1:,1:-1]*( \
              (g.T2[1:-1,1:,1:-1] - g.T2[1:-1,:-1,1:-1]) \
            + (g.T4[1:-1,:,1:] - g.T4[1:-1,:,:-1]) \
            + (g.T6[1:,:,1:-1] - g.T6[:-1,:,1:-1]))

        g.uz_new[1:-1,1:-1,:] = 2*g.uz[1:-1,1:-1,:] \
            - g.uz_old[1:-1,1:-1,:] \
            + g.T2u[1:-1,1:-1,1:]*( \
              (g.T3[1:-1,1:-1,1:] - g.T3[1:-1,1:-1,:-1]) \
            + (g.T4[1:-1,1:,:] - g.T4[1:-1,:-1,:]) \
            + (g.T5[1:,1:-1,:] - g.T5[:-1,1:-1,:]))

    def update_u_BC(self, g, m):

        # self.apply_u_pbc(g)
        self.apply_u_tfbc(g)
        self.apply_u_abc1(g, m)

    def apply_u_pbc(self, g):

        g.ux_new[:,0,:] = g.ux_new[:,-2,:]
        # g.uy_new[:,0,:] = g.uy_new[:,-2,:]
        g.uz_new[:,0,:] = g.uz_new[:,-2,:]

        # g.ux_new[:,-1,:] = g.ux_new[:,1,:]
        g.uy_new[:,-1,:] = g.uy_new[:,1,:]
        # g.uz_new[:,-1,:] = g.uz_new[:,1,:]

    def apply_u_tfbc(self, g):

        g.ux_new[:,1:-1,0] = 2*g.ux[:,1:-1,0] \
            - g.ux_old[:,1:-1,0] \
            + g.T2u[1:,1:-1,0]*( \
              (g.T1[1:,1:-1,0] - g.T1[:-1,1:-1,0]) \
            + (g.T5[:,1:-1,0] - 0) \
            + (g.T6[:,1:,0] - g.T6[:,:-1,0]))

        g.uy_new[1:-1,:,0] = 2*g.uy[1:-1,:,0] \
            - g.uy_old[1:-1,:,0] \
            + g.T2u[1:-1,1:,0]*( \
              (g.T2[1:-1,1:,0] - g.T2[1:-1,:-1,0]) \
            + (g.T4[1:-1,:,0] - 0) \
            + (g.T6[1:,:,0] - g.T6[:-1,:,0]))

        g.uz_new[1:-1,1:-1,0] = 2*g.uz[1:-1,1:-1,0] \
            - g.uz_old[1:-1,1:-1,0] \
            + g.T2u[1:-1,1:-1,0]*( \
              (g.T3[1:-1,1:-1,1] - g.T3[1:-1,1:-1,0]) \
            + (g.T4[1:-1,1:,0] - g.T4[1:-1,:-1,0]) \
            + (g.T5[1:,1:-1,0] - g.T5[:-1,1:-1,0]))

    def apply_u_abc1(self, g, m):

        cl = m.abc_coef_long
        ct = m.abc_coef_tran

        # g.ux_new[0,:,:] = g.ux[1,:,:] + cl*(g.ux_new[1,:,:]-g.ux[0,:,:])
        # g.uy_new[0,:,:] = g.uy[1,:,:] + ct*(g.uy_new[1,:,:]-g.uy[0,:,:])
        # g.uz_new[0,:,:] = g.uz[1,:,:] + ct*(g.uz_new[1,:,:]-g.uz[0,:,:])
        g.ux_new[-1,:,:] = g.ux[-2,:,:] + cl*(g.ux_new[-2,:,:]-g.ux[-1,:,:])
        g.uy_new[-1,:,:] = g.uy[-2,:,:] + ct*(g.uy_new[-2,:,:]-g.uy[-1,:,:])
        g.uz_new[-1,:,:] = g.uz[-2,:,:] + ct*(g.uz_new[-2,:,:]-g.uz[-1,:,:])

        g.ux_new[:,0,:] = g.ux[:,1,:]  + ct*(g.ux_new[:,1,:]-g.ux[:,0,:])
        g.uy_new[:,0,:] = g.uy[:,1,:]  + cl*(g.uy_new[:,1,:]-g.uy[:,0,:])
        g.uz_new[:,0,:] = g.uz[:,1,:]  + ct*(g.uz_new[:,1,:]-g.uz[:,0,:])
        g.ux_new[:,-1,:] = g.ux[:,-2,:] + ct*(g.ux_new[:,-2,:]-g.ux[:,-1,:])
        g.uy_new[:,-1,:] = g.uy[:,-2,:] + cl*(g.uy_new[:,-2,:]-g.uy[:,-1,:])
        g.uz_new[:,-1,:] = g.uz[:,-2,:] + ct*(g.uz_new[:,-2,:]-g.uz[:,-1,:])

        # g.ux_new[:,:,0] = g.ux[:,:,1] + ct*(g.ux_new[:,:,1]-g.ux[:,:,0])
        # g.uy_new[:,:,0] = g.uy[:,:,1] + ct*(g.uy_new[:,:,1]-g.uy[:,:,0])
        # g.uz_new[:,:,0] = g.uz[:,:,1] + cl*(g.uz_new[:,:,1]-g.uz[:,:,0])
        g.ux_new[:,:,-1] = g.ux[:,:,-2] + ct*(g.ux_new[:,:,-2]-g.ux[:,:,-1])
        g.uy_new[:,:,-1] = g.uy[:,:,-2] + ct*(g.uy_new[:,:,-2]-g.uy[:,:,-1])
        g.uz_new[:,:,-1] = g.uz[:,:,-2] + cl*(g.uz_new[:,:,-2]-g.uz[:,:,-1])

    def time_step(self, g):

        g.ux_temp[:,:,:] = g.ux
        g.uy_temp[:,:,:] = g.uy
        g.uz_temp[:,:,:] = g.uz

        g.ux[:,:,:] = g.ux_new
        g.uy[:,:,:] = g.uy_new
        g.uz[:,:,:] = g.uz_new

        g.ux_old[:,:,:] = g.ux_temp
        g.uy_old[:,:,:] = g.uy_temp
        g.uz_old[:,:,:] = g.uz_temp
